package gfk.demo.boot.kotlin.author

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit.jupiter.SpringExtension

/**
 * @author Rashidi Zin
 */
@ExtendWith(SpringExtension::class)
@DataJpaTest
class AuthorRepositoryTests(@Autowired val repository: AuthorRepository) {

    @Test
    fun `When findAll then return all Authors`() {
        repository.save(
                Author("Rudyard Kipling")
        )

        assertThat(repository.findAll()).isNotEmpty
    }

}