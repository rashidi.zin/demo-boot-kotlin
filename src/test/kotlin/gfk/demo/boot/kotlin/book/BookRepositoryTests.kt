package gfk.demo.boot.kotlin.book

import gfk.demo.boot.kotlin.author.Author
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.groups.Tuple.tuple
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.test.context.junit.jupiter.SpringExtension

/**
 * @author Rashidi Zin
 */
@ExtendWith(SpringExtension::class)
@DataJpaTest
class BookRepositoryTests(@Autowired val entityManager: TestEntityManager, @Autowired val repository: BookRepository) {

    @Test
    fun `When findByAuthorName then return authored Books`() {
        val author = entityManager.persistAndFlush(Author("Rudyard Kipling"))

        val book = repository.save(Book(author, "If"))

        assertThat(repository.findAllByAuthorName(author.name))
                .hasSize(1)
                .extracting("author.name", "title")
                .contains(tuple(author.name, book.title))
    }
}