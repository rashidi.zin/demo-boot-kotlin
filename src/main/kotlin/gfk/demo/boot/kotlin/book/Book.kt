package gfk.demo.boot.kotlin.book

import gfk.demo.boot.kotlin.author.Author
import javax.persistence.*

/**
 * @author Rashidi Zin
 */
@Entity
data class Book(
        @ManyToOne @JoinColumn val author: Author,
        val title: String,
        @Id @GeneratedValue val id: Long? = null
)