package gfk.demo.boot.kotlin.book

import org.springframework.data.jpa.repository.JpaRepository

/**
 * @author Rashidi Zin
 */
interface BookRepository : JpaRepository<Book, Long> {

    fun findAllByAuthorName(name: String): List<Book>

}