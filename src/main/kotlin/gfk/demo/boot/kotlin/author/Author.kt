package gfk.demo.boot.kotlin.author

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

/**
 * @author Rashidi Zin
 */
@Entity
data class Author(
        val name: String,
        val description: String? = null,
        @Id @GeneratedValue val id: Long? = null
)