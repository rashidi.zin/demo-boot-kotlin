package gfk.demo.boot.kotlin.author

import org.springframework.data.jpa.repository.JpaRepository

/**
 * @author Rashidi Zin
 */
interface AuthorRepository : JpaRepository<Author, Long>